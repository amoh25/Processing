import datetime
import json
import cryptography

import connexion
from connexion import NoContent
import swagger_ui_bundle

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from base import Base
from buy import Buy
from sell import Sell
import yaml
import logging
import logging.config

# NEW STUFF
with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())
DB_ENGINE = create_engine(f"mysql+pymysql://{app_config['user']}:{app_config['password']}@{app_config['hostname']}:{app_config['port']}/{app_config['db']}")
Base.metadata.bind = DB_ENGINE
DB_SESSION = sessionmaker(bind=DB_ENGINE)

# Endpoints
def buy(body):
    session = DB_SESSION()
    buy = Buy(buy_id=body['buy_id'], 
                item_name=body['item_name'] ,
                item_price=body['item_price'], 
                buy_qty=body['buy_qty'], 
                trace_id=body['trace_id'])

    session.add(buy)
    session.commit()
    return NoContent, 201
# end

def get_buys(timestamp):
    data = []
    with DB_SESSION.begin() as session:
        rows = session.query(Buy).filter(Buy.date_created >= timestamp)
        for row in rows:
            data.append(row.to_dict())
    logging.info("get_buys: timestamp: %s, number of results: %s", timestamp, len(data))
    return data, 200

def sell(body):
    session = DB_SESSION()
    sell = Sell(sell_id=body['sell_id'], item_name=body['item_name'] ,item_price=body['item_price'], sell_qty=body['sell_qty'], trace_id=body['trace_id'])
    session.add(sell)
    session.commit()

    return NoContent, 201
# end

def get_sells(timestamp):
    session = DB_SESSION()
    rows = session.query(Sell).filter(Sell.date_created >= timestamp)
    data = []
    for row in rows:
        data.append(row.to_dict())
    session.close()
    logging.info("get_sells: timestamp: %s, number of results: %s", timestamp, len(data))
    return data, 200

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basic')
logger.info("ASSIGNMENT 3")

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api('openapi.yaml', base_path="/processing" ,strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8100)
